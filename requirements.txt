pip install -r requirements.txt

appdirs==1.4.4
atomicwrites==1.3.0
attrs==19.1.0
backcall==0.2.0
beautifulsoup4==4.9.3
blis==0.7.4
bpemb==0.3.2
cached-property==1.5.2
calmjs.parse==1.2.5
catalogue==1.0.0
certifi==2020.6.20
cffi==1.14.4
chardet==4.0.0
click==7.1.2
cloudpickle==1.6.0
cycler==0.10.0
cymem==2.0.5
Cython==0.29.21
dataclasses==0.6
decorator==4.4.2
deep-forest==0.1.3
Deprecated==1.2.12
en-core-web-sm @ https://github.com/explosion/spacy-models/releases/download/en_core_web_sm-2.3.1/en_core_web_sm-2.3.1.tar.gz
filelock==3.0.12
flair==0.8.0.post1
ftfy==5.9
funcy==1.17
future==0.18.2
gdown==3.12.2
gensim==3.8.3
greenlet==1.1.2
h5py==3.0.0
html5lib==1.1
huggingface-hub==0.0.7
hyperopt==0.2.5
idna==2.10
importlib-metadata==3.7.3
ipython @ file:///tmp/build/80754af9/ipython_1604101197014/work
ipython-genutils==0.2.0
Janome==0.4.1
jedi @ file:///tmp/build/80754af9/jedi_1598371611696/work
Jinja2==3.0.3
joblib @ file:///home/conda/feedstock_root/build_artifacts/joblib_1601671685479/work
js2xml==0.4.0
kiwisolver @ file:///tmp/build/80754af9/kiwisolver_1604014535162/work
konoha==4.6.4
langdetect==1.0.8
liwc-text-analysis==1.0.2
llvmlite==0.35.0
lxml==4.6.3
MarkupSafe==2.0.1
matplotlib @ file:///tmp/build/80754af9/matplotlib-base_1603378225747/work
mkl-fft==1.2.0
mkl-random==1.1.1
mkl-service==2.3.0
more-itertools==7.2.0
mpi4py @ file:///home/conda/feedstock_root/build_artifacts/mpi4py_1602248747679/work
mpld3==0.3
multitasking==0.0.9
murmurhash==1.0.5
networkx==2.5
nibabel==3.2.0
nilearn==0.6.2
nitime==0.8.1
nltk==3.5
nsepy==0.8
numba==0.52.0
numexpr==2.8.1
numpy @ file:///tmp/build/80754af9/numpy_and_numpy_base_1603570489231/work
olefile==0.46
overrides==3.1.0
packaging==19.1
pandas==1.4.0
parso==0.7.0
patsy==0.5.1
pexpect==4.8.0
pickleshare==0.7.5
Pillow @ file:///tmp/build/80754af9/pillow_1603822255246/work
plac==1.1.3
pluggy==0.12.0
ply==3.11
pooch==1.3.0
preshed==3.0.5
prompt-toolkit @ file:///tmp/build/80754af9/prompt-toolkit_1602688806899/work
psutil @ file:///tmp/build/80754af9/psutil_1598370257551/work
ptyprocess==0.6.0
py==1.8.0
pycparser==2.20
pydicom==2.1.0
pydub==0.24.1
Pygments @ file:///tmp/build/80754af9/pygments_1604103097372/work
pyLDAvis==3.3.1
pyparsing==2.4.2
pyqode.core==2.12.1
pyqode.qt==2.10.0
pytest==5.1.2
python-dateutil==2.8.1
pytz==2020.1
regex==2020.11.13
requests==2.25.1
resampy==0.2.2
researchpy==0.3.2
sacremoses==0.0.43
scikit-learn @ file:///home/conda/feedstock_root/build_artifacts/scikit-learn_1604232448678/work
scipy @ file:///tmp/build/80754af9/scipy_1597686649129/work
seaborn @ file:///tmp/build/80754af9/seaborn_1600553570093/work
segtok==1.5.10
selenium==3.141.0
sentencepiece==0.1.95
sip==4.19.13
six==1.12.0
sklearn==0.0
smart-open==4.2.0
SoundFile==0.10.3.post1
soupsieve==2.1
spacey==0.1.1
spacy==2.3.5
srsly==1.0.5
tabulate==0.8.9
textblob==0.15.3
thinc==7.4.5
threadpoolctl @ file:///tmp/tmp79xdzxkt/threadpoolctl-2.1.0-py3-none-any.whl
tokenizers==0.10.1
torch==1.7.0
torchvision==0.8.1
tornado==6.0.4
tqdm==4.56.0
traitlets @ file:///tmp/build/80754af9/traitlets_1602787416690/work
transformers==4.3.0
typing-extensions @ file:///tmp/build/80754af9/typing_extensions_1598376058250/work
urllib3==1.26.3
wasabi==0.8.1
wcwidth==0.1.7
webencodings==0.5.1
wordcloud==1.8.1
wrapt==1.12.1
zipp==0.6.0
