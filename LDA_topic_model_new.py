import numpy as np
import pandas as pd
import seaborn as sns
import nltk
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from nltk.stem import WordNetLemmatizer
from sklearn.preprocessing import LabelBinarizer
from bs4 import BeautifulSoup
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
from sklearn.neighbors import KNeighborsClassifier
from nltk.tokenize import word_tokenize, sent_tokenize, WhitespaceTokenizer, wordpunct_tokenize, TreebankWordTokenizer
from nltk.stem import PorterStemmer
import unicodedata, re, string
import gensim
from gensim import corpora
import pyLDAvis
import pyLDAvis.gensim_models
import matplotlib.pyplot as plt
from sklearn.decomposition import LatentDirichletAllocation
import io

import os
import warnings

warnings.filterwarnings('ignore')

stopword_list = nltk.corpus.stopwords.words('english')
#Additional words based on the dataset
stopword_list.extend(['dont', 'know', 'still', 'its', 'much'])


def strip_html(text):
    soup = BeautifulSoup(text, "html.parser")
    return soup.get_text()


# Removing the square brackets
def remove_between_square_brackets(text):
    return re.sub('\[[^]]*\]', '', text)


# Removing the noisy text
def denoise_text(text):
    text = strip_html(text)
    text = remove_between_square_brackets(text)
    return text


# 2) Remove special characters
def rem_special_char(text, remove_digits=True):
    pattern = r'[^a-zA-z0-9\s]'
    text = re.sub(pattern, '', text)
    return text


# 3) Remove all upper case words
def lower_case(text):
    text = text.lower()
    return text


# 4) Stemming
def stemmer(text):
    ps = PorterStemmer()
    text = ' '.join([ps.stem(word) for word in text.split()])
    return text


# 5) Lemmatization
def lemmo(text):
    lemma = WordNetLemmatizer()
    text = lemma.lemmatize(text)
    return text


# 6) Stop-words - Tokenization
def stop_word_token(text, is_lower_case=True):
    tk = WhitespaceTokenizer()
    text = tk.tokenize(text)
    filtered_text = [w for w in text if w not in stopword_list]
    filtered_text = ' '.join(filtered_text)
    return filtered_text

# load csv file
gc_df = pd.read_csv('globalcount_data.csv')

# remove any missing responses -- optional
gc_df = gc_df[gc_df['progress_10_years_tr'].notna()]

# See what percentage of responses are null
gc_df['progress_10_years_tr'].isnull().sum() / len(gc_df)

# Data cleaning for topic modeling
gc_df['progress_10_years_tr'] = gc_df['progress_10_years_tr'].str.strip()
gc_df['progress_10_years_tr'] = gc_df['progress_10_years_tr'].apply(denoise_text)
gc_df['progress_10_years_tr'] = gc_df['progress_10_years_tr'].apply(rem_special_char)
gc_df['progress_10_years_tr'].replace(r'\\', '', regex=True, inplace=True)
gc_df['progress_10_years_tr'] = gc_df['progress_10_years_tr'].apply(lower_case)
# gc_df['progress_10_years_tr'] = gc_df['progress_10_years_tr'].apply(stemmer)
# gc_df['progress_10_years_tr'] = gc_df['progress_10_years_tr'].apply(lemmo)
gc_df['progress_10_years_tr'] = gc_df['progress_10_years_tr'].apply(stop_word_token)

# place data into an array
dataset = [d.split() for d in gc_df['progress_10_years_tr']]

# create dictionary with corpora
dictionary = corpora.Dictionary(dataset)

# Bag to words creation
doc_term_matrix = [dictionary.doc2bow(res) for res in dataset]

# Create LDA object from gensim library
LDA = gensim.models.ldamodel.LdaModel


#build LDA model
lda_model = LDA(corpus=doc_term_matrix, id2word=dictionary, num_topics=3, random_state=76, passes=40, alpha='auto')

#print topics
lda_model.print_topics()

# (0,
#  '0.025*"violence" + 0.015*"women" + 0.012*"domestic" + 0.012*"woman" + 0.012*"freedom" + 0.011*"without" + 0.009*"children" + 0.008*"law" + 0.008*"right" + 0.006*"absence"'),
# (1,
#  '0.048*"women" + 0.027*"rights" + 0.017*"progress" + 0.014*"years" + 0.012*"human" + 0.012*"better" + 0.010*"women`s" + 0.010*"10" + 0.010*"good" + 0.008*"change"'),
# (2,
#  '0.050*"women" + 0.043*"equal" + 0.021*"equality" + 0.017*"gender" + 0.016*"pay" + 0.014*"work" + 0.013*"men" + 0.012*"rights" + 0.012*"violence" + 0.012*"education"')]

#visualize
pyLDAvis.enable_notebook()
vis = pyLDAvis.gensim_models.prepare(lda_model, doc_term_matrix, dictionary)

print('\n Perplexity score:', lda_model.log_perplexity(doc_term_matrix, total_docs = 10000))

#Compute Coherence Score
from gensim.models.coherencemodel import CoherenceModel
coherence_model_lda = CoherenceModel(model = lda_model, texts = dataset, dictionary = dictionary, coherence = 'c_v')
coherence_lda = coherence_model_lda.get_coherence()
print('\n Coherence Score is :', coherence_lda)


#Save the html file to upload to site
pyLDAvis.save_html(vis, 'index.html')
