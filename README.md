# global_count_NLP

This was a datathon, put on by Data for Good: Waterloo. Global_count.csv is a survey dataset from Facebook, asking women from various countries about women's rights. My job was to examine the free text response column ("What would you like to see progress in the next 10 years for women's rights?"). I decided to create word-clouds for the different age demographics, and used a topic model to cluster various documents into "topics". 

## Dependencies

- Please refer to the requirements.txt to download all the neccessary libraries

## Wordclouds

- After cleaning the data in wordclouds_age.py, I generated wordcloud figures of the top 50 words for each age demographics for women who spoke about women's rights progress in 10 years. Here are the results of some of those wordclouds

- Keep in mind, I removed the missing entries for this analysis, but I have another one that includes missing responses


![ ](word_clouds/18 to 24.png)

![ ](word_clouds/35 to 44.png)

![ ](word_clouds/45 to 54.png)

![ ](word_clouds/65 to 74.png)

![ ](word_clouds/75 or older.png)

## Topic Modeling

[Interactive topic model](https://gclda.000webhostapp.com/#topic=0&lambda=1&term=)

- We want to examine the major topics/keywords used in the free text column
- Topic modeling is a probabilistic process that clusters words from documents into topics


![ ](topic_model.png)

- Here is the code to my model (I used 3 topics, but will optimize the right number of topics to choose)

class topic_model:

    lda_model = LDA(corpus=doc_term_matrix, id2word=dictionary, num_topics=3, random_state=76, passes=40, alpha='auto')


- Perplexity score: -7.5
- Coherence score: 0.53






