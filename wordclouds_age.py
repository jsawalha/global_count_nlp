import numpy as np
import pandas as pd
import seaborn as sns
import nltk
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from nltk.stem import WordNetLemmatizer
import spacy
import matplotlib.pyplot as plt
import en_core_web_sm
from collections import defaultdict
from nltk.tokenize import word_tokenize,sent_tokenize, WhitespaceTokenizer, wordpunct_tokenize, TreebankWordTokenizer
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import re



#Cleaning functions

#Stopword list
stopword_list=nltk.corpus.stopwords.words('english')

#Add this IF YOU don't want to see NaN fillings in wordcloud
stopword_list.append('missing')


#Cleaning functions
# #6) Stop-words - Tokenization
def stop_word_token(text, is_lower_case = True):
    tk = WhitespaceTokenizer()
    text = tk.tokenize(text)
    filtered_text = [w for w in text if w not in stopword_list]
    filtered_text = ' '.join(filtered_text)
    return filtered_text

def lower_case(text):
    text = text.lower()
    return text

def lemmo(text):
    lemma = WordNetLemmatizer()
    text = lemma.lemmatize(text)
    return text

# 2) Remove special characters
def rem_special_char(text, remove_digits=True):
    pattern = r'[^a-zA-z0-9\s]'
    text = re.sub(pattern, '', text)
    return text


#seaborn setting for visualization
sns.set_context("poster")
sns.set(font_scale = 2)

#load dataset
gc_df = pd.read_csv('../dfg_global/globalcount_data.csv')

#Look at breakdown of age
gc_df['age'].value_counts(dropna=False)

#plot age demographic count
sns.countplot(gc_df['age'].fillna('Missing'))


#Clean nan values, add the word "missing" in this text columns
gc_df['progress_10_years_tr'].fillna('Missing', inplace=True)
gc_df['age'].fillna('NA', inplace=True)


#Clean empty spaces for text column
#Cleaning text data...
gc_df['progress_10_years_tr'] = gc_df['progress_10_years_tr'].str.replace('  ', '')
gc_df['progress_10_years_tr'] = gc_df['progress_10_years_tr'].apply(rem_special_char)
gc_df['progress_10_years_tr'].replace(r'\\','', regex=True, inplace=True)
gc_df['progress_10_years_tr'] = gc_df['progress_10_years_tr'].apply(lower_case)
gc_df['progress_10_years_tr'] = gc_df['progress_10_years_tr'].apply(lemmo)
gc_df['progress_10_years_tr'] = gc_df['progress_10_years_tr'].apply(stop_word_token)



#loop to create word clouds for all age groups
age_demo = ['18 to 24', '25 to 34', '35 to 44', '45 to 54', '55 to 64', '65 to 74', '75 or older', 'NA']

age_labels = ['youngest', 'young', 'young_mid', 'middle', 'mid_old', 'mid_older', 'oldest', 'missing']

ovr = len(" ".join(review for review in gc_df['progress_10_years_tr']))

for k, l in zip(age_demo, age_labels):
    concat = " ".join(review for review in gc_df[gc_df['age'] == k]['progress_10_years_tr'])
    print(f"There are {len(concat)} words in the {k, l} group. PERCENTAGE OF OVR WORDS: {np.round((len(concat) / ovr) * 100)}")
    wordcloud = WordCloud(width=600,height=390,stopwords=stopword_list, collocations=True, colormap='tab20c', max_words=50).generate(concat)
    # Display the generated image:
    plt.figure()
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis("off")
    plt.title(f"Top 50 words: '{k}' group")
    plt.show()
    plt.savefig(f'../dfg_global/word_clouds/missing_included/{k}.png', bbox_inches = "tight")















