import numpy as np
import pandas as pd
import seaborn as sns

import warnings
warnings.filterwarnings('ignore')


gc_df = pd.read_csv('../dfg_global/globalcount_data.csv')

#description of data, general
sum_gc = gc_df.describe()
#53 columns are numeric
